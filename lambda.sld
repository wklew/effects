;;; (effects lambda) --- Higher-order effects

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides effects and handlers for modeling
;;; higher-order effects: substitution, abstraction and application.

;;; Code:

(define-library (effects lambda)

  (export var lam app def fix val letreq norm unbound-failure)

  (import (scheme base)
          (scheme case-lambda)
          (effects base)
          (effects tuple)
          (effects io)
          (effects partial)
          (effects logic)
          (effects syntax)
          (containers dictionary))

  (cond-expand
   (guile (import (srfi srfi-1)
                  (srfi srfi-69)))
   (chibi (import (srfi 1)
                  (srfi 69)))
   (chicken (import (srfi-1)
                    (srfi-69))))

  (begin

    (define environment-prompt (make-prompt-tag 'environment))
    (define lambda-prompt (make-prompt-tag 'lambda))
    (define unbound-failure (make-prompt-tag 'unbound))

    ;; Variable substitution:
    ;; look up a name in the current environment;
    ;; if no binding is found, raise an `unbound-failure`

    (define (var name)
      (get environment-prompt
        (lambda (t)
          (guard (e ((dictionary-lookup-error? e)
                     (throw unbound-failure name)))
            (dictionary-lookup name t)))))

    ;; Lambda abstraction:
    ;; label a request with a name,
    ;; to be bound within the body of the request

    ;; See below for a hygienic version

    (define-record-type <abstraction>
      (make-abstraction name req)
      abstraction?
      (name abstraction-name)
      (req abstraction-request))

    (define (%lam1 name req)
      (label lambda-prompt name req))

    (define (%lam names req)
      (fold-right %lam1 req names))

    ;; Function application:
    ;; evaluate the operator to yield a closure,
    ;; then evaluate the body of the closure
    ;; in an environment with a binding for the operand

    ;; Note that we have to worry about neutral terms
    ;; See more on that below

    (define (app1 rator rand)
      (mlet ((f rator)
             (x rand))
        (%val (closure-request f)
              (dictionary-insert (closure-name f)
                                 x
                                 (closure-bindings f)))))

    (define (app fun-req . arg-reqs)
      (fold (lambda (arg-req fun-req)
              (app1 fun-req arg-req))
            fun-req
            arg-reqs))

    ;; Definition:
    ;; extend the environment with a name bound to a request

    ;; Note that duplicate names result in a `choice`
    ;; This implements Prolog-style nondeterministic definitions

    ;; See below for a hygienic version

    (define (%def name req)
      (get environment-prompt
        (lambda (t)
          (put environment-prompt
            (dictionary-insert choice name req t)))))

    ;; Lambda evaluator

    ;; See below for a hygienic version

    (define-record-type <closure>
      (make-closure bnd name req)
      closure?
      (bnd closure-bindings)
      (name closure-name)
      (req closure-request))

    (define %val
      (case-lambda
        ((req)
         (%val req (dictionary-empty)))
        ((req bindings)
         (let loop ((bindings bindings)
                    (req req))
           (define (loop1 req)
             (loop bindings req))
           (handle req
             `(,(input-handler
                 environment-prompt
                 loop1
                 (lambda (cont)
                   (loop bindings (cont bindings))))
               ,(output-handler
                 environment-prompt
                 loop1
                 (lambda (thunk bindings)
                   (loop bindings (thunk))))
               ,(tuple-handler
                 lambda-prompt
                 loop1
                 (lambda (cont name req)
                   (loop bindings
                         (cont
                          (make-closure bindings name req)))))))))))

    ;; Effectful interface for generating fresh terms, used below

    (define fresh-name-prompt (make-prompt-tag 'fresh-name))
    (define fresh-name (get fresh-name-prompt))
    (define (gen-fresh-names req) (input-iota fresh-name-prompt req))

    ;; Hygienic versions of lambda, define and eval
    ;; which introduce fresh names

    (define-syntax with-names
      (syntax-rules ()
        ((_ (name ...) expr ...)
         (mlet ((name fresh-name) ...)
           (mbegin expr ...)))))

    (define-syntax lam
      (syntax-rules ()
        ((_ (name ...) expr ...)
         (with-names (name ...)
           (%lam (list name ...)
                 (mbegin expr ...))))))

    (define-syntax def
      (syntax-rules ()
        ((_ name req ...)
         (%def name (mbegin req ...)))))

    (define-syntax mbegin*
      (syntax-rules (def)
        ((_) unit)
        ((_ req) req)
        ((_ (def name req ...) expr ...)
         (let ((name (hash 'name)))
           (mbegin
            (def name req ...)
            (mbegin* expr ...))))
        ((_ req req* ...)
         (mbegin req (mbegin* req* ...)))))

    (define-syntax val
      (syntax-rules ()
        ((_ req ...)
         (gen-fresh-names
          (%val (mbegin* req ...))))))

    ;; Y-combinator or fixed-point combinator
    ;; Our first embedded lambda term
    ;; This is the eta-expanded version; our host language is strict

    (define fix
      (lam (fun arg)
        (mlet ((f (var fun)))
          (app f
               (app fix f)
               (var arg)))))

    ;; Neutral terms
    ;; This section implements normalization by evaluation

    (define neutral-prompt (make-prompt-tag 'neutral-prompt))

    (define (neutral name)
      (request neutral-prompt name))

    (define (neutral-handler proc)
      (make-handler neutral-prompt proc))

    (define (unneutral req)
      (handle req
        `(,(neutral-handler
            (lambda (cont name)
              (mlet ((x (var name)))
                (unneutral (cont x))))))))

    (define (unval req)
      (let loop ((req req))
        (mlet ((x req))
          (if (not (closure? x))
              x
              (mlet ((name fresh-name))
                (%lam1 name
                       (loop
                        (unneutral
                         (mlet ((y (neutral name)))
                           (%val (closure-request x)
                                 (dictionary-insert (closure-name x)
                                                    y
                                                    (closure-bindings x))))))))))))

    (define (norm req)
      (unval (%val req)))

    ;; Let binding, for reference
    ;; Not exported since we prefer to use syntactic `mlet`

    (define letreq
      (lam (env expr)
        (mlet ((bnds (var env)))
          (fold-right (lambda (bnd acc)
                        (app1 (%lam1 (car bnd)
                                     acc)
                              (cdr bnd)))
                      (var expr)
                      bnds))))))
