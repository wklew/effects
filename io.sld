;;; (effects io) --- Input and output

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides effects and handlers which model reading from,
;;; writing to and updating dynamic resources.

;;; Code:

(define-library (effects io)

  (export get
          put
          lookup-handler
          insertion-handler
          input-handler
          output-handler
          input
          output
          update
          input-request
          input-const
          input-exhausted-failure
          input-list
          input-unfold
          input-await
          input-iota
          writer-log-value
          writer-log-output
          output-request
          output-last
          output-fold
          output-yield
          output-pass
          output-list
          output-string
          output-sum
          output-product
          state-result-value
          state-result-final
          update-state
          update-exchange
          update-list)

  (import (scheme base)
          (scheme case-lambda)
          (effects base)
          (effects partial))

  (begin

    (define input-prompt (make-prompt-tag 'input))
    (define output-prompt (make-prompt-tag 'output))

    (define-record-type <lookup>
      (make-lookup tag)
      lookup?
      (tag lookup-tag))

    (define-record-type <insertion>
      (make-insertion tag val)
      insertion?
      (tag insertion-tag)
      (val insertion-value))

    (define get
      (case-lambda
        ((tag)
         (get tag identity))
        ((tag cont)
         (request input-prompt (make-lookup tag)
           cont))))

    (define put
      (case-lambda
        ((tag req)
         (put tag req unit))
        ((tag req thunk)
         (bind req
           (lambda (val)
             (request output-prompt (make-insertion tag val)
               (lambda (_)
                 (thunk))))))))

    (define (lookup-handler proc)
      (make-handler input-prompt
        (lambda (cont lkup)
          (proc cont (lookup-tag lkup)))))

    (define (insertion-handler proc)
      (make-handler output-prompt
        (lambda (cont ins)
          (proc (lambda () (cont unit))
                (insertion-tag ins)
                (insertion-value ins)))))

    (define (input-handler tag loop proc)
      (lookup-handler
       (lambda (cont tag*)
         (if (eq? tag tag*)
             (proc cont)
             (get tag* (compose loop cont))))))

    (define (output-handler tag loop proc)
      (insertion-handler
       (lambda (thunk tag* val)
         (if (eq? tag tag*)
             (proc thunk val)
             (put tag* val (compose loop thunk))))))

    (define (input tag loop req proc . rest)
      (apply handle
             req
             `(,(input-handler tag loop proc))
             rest))

    (define (output tag loop req proc . rest)
      (apply handle
             req
             `(,(output-handler tag loop proc))
             rest))

    (define (update tag loop req input-proc output-proc . rest)
      (apply handle
             req
             `(,(input-handler tag loop input-proc)
               ,(output-handler tag loop output-proc))
             rest))

    (define (input-request tag in-req req)
      (let loop ((req req))
        (input tag
               loop
               req
               (lambda (cont)
                 (bind in-req
                   (lambda (x)
                     (loop (cont x))))))))

    (define (input-await tag thunk req)
      (let loop ((req req))
        (input tag
               loop
               req
               (lambda (cont)
                 (loop (cont (thunk)))))))

    (define (input-const tag val req)
      (let loop ((req req))
        (input tag
               loop
               req
               (lambda (cont)
                 (loop (cont val))))))

    (define input-exhausted-failure (make-prompt-tag 'input-exhausted))

    (define (input-list tag lst req)
      (let loop ((lst lst)
                 (req req))
        (input tag
               (lambda (req)
                 (loop lst req))
               req
               (lambda (cont)
                 (if (null? lst)
                     (throw input-exhausted-failure)
                     (loop (cdr lst)
                           (cont (car lst))))))))

    (define (input-unfold tag next seed req)
      (let loop ((seed seed)
                 (req req))
        (input tag
               (lambda (req)
                 (loop seed req))
               req
               (lambda (cont)
                 (let-values (((x seed) (next seed)))
                   (loop seed (cont x)))))))

    (define (input-iota tag req)
      (let loop ((n 0)
                 (req req))
        (input tag
               (lambda (req)
                 (loop n req))
               req
               (lambda (cont)
                 (loop (+ n 1) (cont n))))))

    (define (output-request tag proc req)
      (let loop ((req req))
        (output tag
                loop
                req
                (lambda (thunk x)
                  (bind (proc x)
                    (lambda (_)
                      (loop (thunk))))))))

    (define-record-type <writer-log-null>
      (make-writer-log-null)
      writer-log-null?)

    (define-record-type <writer-log>
      (make-writer-log val acc)
      writer-log?
      (val writer-log-value)
      (acc writer-log-output))

    (define (output-last tag req)
      (let loop ((out (make-writer-log-null))
                 (req req))
        (output tag
                (lambda (req)
                  (loop out req))
                req
                (lambda (thunk out)
                  (loop out (thunk)))
                (lambda (x)
                  (make-writer-log x out)))))

    (define (output-fold tag combine base req)
      (let loop ((acc base)
                 (req req))
        (output tag
                (lambda (req)
                  (loop acc req))
                req
                (lambda (thunk x)
                  (loop (combine acc x)
                        (thunk)))
                (lambda (x)
                  (make-writer-log x acc)))))

    (define (output-yield tag proc req)
      (bind (output-fold tag
                         (lambda (_ val)
                           (proc val))
                         unit
                         req)
        writer-log-value))

    (define (output-pass tag req)
      (output-yield tag (lambda (_) unit) req))

    (define (output-list tag req)
      (output-fold tag (lambda (xs x) (cons x xs)) '() req))

    (define (output-string tag req)
      (output-fold tag string-append "" req))

    (define (output-sum tag req)
      (output-fold tag + 0 req))

    (define (output-product tag req)
      (output-fold tag * 1 req))

    (define-record-type <state-result>
      (make-state-result val fin)
      state-result?
      (val state-result-value)
      (fin state-result-final))

    (define (update-state tag st req)
      (let loop ((st st)
                 (req req))
        (update tag
                (lambda (req)
                  (loop st req))
                req
                (lambda (cont)
                  (loop st (cont st)))
                (lambda (thunk st)
                  (loop st (thunk)))
                (lambda (x)
                  (make-state-result x st)))))

    (define (update-exchange tag read-thunk write-proc req)
      (let loop ((req req))
        (update tag
                loop
                req
                (lambda (cont)
                  (loop (cont (read-thunk))))
                (lambda (thunk x)
                  (write-proc x)
                  (loop (thunk))))))

    (define (update-list tag lst req)
      (let loop ((lst lst)
                 (req req))
        (update tag
                (lambda (req)
                  (loop lst req))
                req
                (lambda (cont)
                  (if (null? lst)
                      (loop '() (cont (eof-object)))
                      (loop (cdr lst)
                            (cont (car lst)))))
                (lambda (thunk x)
                  (loop (cons x lst)
                        (thunk))))))))
