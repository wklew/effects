(define-library (effects concurrency)

  (export rendezvous)

  (import (scheme base)
          (scheme write)
          (effects base)
          (effects io))

  (begin

    (define (rendezvous requester responder request-chan respond-chan)
      (let loop ((responded? #t)
                 (requester requester)
                 (responder responder))
        (handle requester
          `(,(output-handler
              request-chan
              (lambda (thunk x)
                (display "Sending request over ")
                (write request-chan)
                (display ": ")
                (write x)
                (newline)
                (handle responder
                  `(,(input-handler
                      request-chan
                      (lambda (cont)
                        (display "Received request over ")
                        (write request-chan)
                        (newline)
                        (loop #f
                              (thunk)
                              (cont x))))))))
            ,(input-handler
              respond-chan
              (lambda (cont)
                (display "Receiving response over ")
                (write respond-chan)
                (newline)
                (handle responder
                  `(,(output-handler
                      respond-chan
                      (lambda (thunk x)
                        (display "Sent response over ")
                        (write respond-chan)
                        (display ": ")
                        (write x)
                        (newline)
                        (loop #t
                              (cont x)
                              (thunk)))))))))
          (lambda (x)
            (if responded?
                (begin
                  (display "Returning: ")
                  (write x)
                  (newline)
                  (pure x))
                (begin
                  (display "Awaiting response")
                  (newline)
                  responder))))))))
