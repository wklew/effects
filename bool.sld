;;; (effects bool) --- Boolean logic

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library defines an effect for pure boolean logic, useful for testing
;;; in combination with other effects.

;;; Code:

(define-library (effects bool)

  (export bool
          true
          false
          neg
          conj
          disj
          impl
          run-bool
          quote-bool)

  (import (scheme base)
          (scheme cxr)
          (effects base)
          (effects syntax))

  (begin

    (define bool-prompt (make-prompt-tag 'bool))

    (define (bool p)
      (request bool-prompt `(bool ,p)))

    (define (neg req)
      (mlet ((p req))
        (request bool-prompt `(neg ,p))))

    (define (conj req1 req2)
      (mlet ((p req1)
             (q req2))
        (request bool-prompt `(conj ,p ,q))))

    (define (disj req1 req2)
      (mlet ((p req1)
             (q req2))
        (request bool-prompt `(disj ,p ,q))))

    ;; Derived

    (define (impl req1 req2)
      (neg (conj req1 (neg req2))))

    ;; Handlers

    (define (bool-handler proc)
      (make-handler bool-prompt
        proc))

    (define (run-bool req)
      (handle req
        `(,(bool-handler
            (lambda (cont expr)
              (run-bool
               (cont
                (let ((rator (car expr))
                      (rand (cdr expr)))
                  (cond
                   ((eq? rator 'bool)
                    (car rand))
                   ((eq? rator 'neg)
                    (not (car rand)))
                   ((eq? rator 'conj)
                    (and (car rand)
                         (cadr rand)))
                   ((eq? rator 'disj)
                    (or (car rand)
                        (cadr rand))))))))))))

    (define (quote-bool req)
      (handle req
        `(,(bool-handler
            (lambda (cont op)
              (quote-bool (cont op)))))))))
