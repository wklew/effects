;;; (effects probability) --- Probabilistic logic programming

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library combines the logic and tuple effects to model Bayesian
;;; networks, which allows for probabilistic logic programming.

;;; Code:

(define-library (effects probability)

  (export prob
          distribute
          alist->distribution
          flip
          uniform
          geometric
          infer
          tabulate
          tabulate-bbtree
          tabulate-hamt
          collect-distribution
          collect-distribution-sorted
          collect-distribution-hashed)

  (import (scheme base)
          (scheme case-lambda)
          (effects base)
          (effects functor)
          (effects logic)
          (effects tuple)
          (effects io))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chicken (import (srfi-1)))
   (chibi (import (srfi 1))))

  (cond-expand
   ((library (pfds bounded-balance-tree))
    (import (pfds bounded-balance-tree)))
   ((library (pfds bbtrees))
    (import (pfds bbtrees))))

  (cond-expand
   ((library (pfds hash-array-mapped-trie))
    (import (pfds hash-array-mapped-trie)))
   ((library (pfds hamts))
    (import (pfds hamts))))

  (begin

    ;; Probabilistic primitives

    (define probability-label (make-prompt-tag 'probability))

    (define (prob wt req)
      (label probability-label wt req))

    (define (distribute alist)
      (fold (lambda (p acc)
              (branch acc (prob (car p) (cdr p))))
            (fail)
            alist))

    (define (alist->distribution alist)
      (fold (lambda (p acc)
              (branch acc (prob (car p) (pure (cdr p)))))
            (fail)
            alist))

    ;; Generating prior distributions

    (define (flip wt heads tails)
      (branch (prob wt heads)
              (prob (- 1 wt) tails)))

    (define uniform
      (case-lambda
        ((count) (uniform count 0 1))
        ((count start) (uniform count start 1))
        ((count start step)
         (let loop ((n 0)
                    (req (fail)))
           (if (>= n count)
               req
               (loop (+ n 1)
                     (branch req
                             (prob (/ 1 count)
                                   (pure (+ start (* n step)))))))))))

    (define (%geometric wt)
      (let ((xwt (- 1 wt)))
        (let loop ((n 0))
          (branch (prob wt (pure n))
                  (prob xwt (loop (+ n 1)))))))

    (define (geometric-bounded wt count)
      (let* ((xwt (- 1 wt))
             (tot (let loop ((acc 1)
                             (n (+ count 1)))
                    (if (zero? n)
                        acc
                        (loop (* xwt acc)
                              (- n 1)))))
             (norm (- 1 tot)))
        (let loop ((acc (fail))
                   (nwt (/ wt norm))
                   (i 0))
          (if (> i count)
              acc
              (loop (branch acc (prob nwt (pure i)))
                    (* nwt xwt)
                    (+ i 1))))))

    (define geometric
      (case-lambda
        ((wt)
         (%geometric wt))
        ((wt count)
         (geometric-bounded wt count))))

    ;; Tabulating posterior distributions

    (define probability-ledger (make-prompt-tag 'probability-ledger))

    (define (infer req)
      (tuple-product probability-label req))

    (define (%tabulate insert empty req)
      (iterate (lambda (node req)
                 (bind req
                   (lambda (acc)
                     (let ((wt (node-label node))
                           (x (node-value node)))
                       (put probability-ledger (pure wt)
                         (lambda ()
                           (insert wt x acc)))))))
               empty
               req))

    (define tabulate
      (case-lambda
        ((insert empty req)
         (%tabulate insert
                    empty
                    (infer req)))
        ((insert empty req count)
         (%tabulate insert
                    empty
                    (logic-take count (infer req))))))

    (define (tabulate-bbtree < req . rest)
      (apply tabulate
             (lambda (wt val bbtree)
               (pure (bbtree-update bbtree val (lambda (x) (+ wt x)) 0)))
             (pure (make-bbtree <))
             req
             rest))

    (define (tabulate-hamt hash eqv? req . rest)
      (apply tabulate
             (lambda (wt val hamt)
               (pure (hamt-update hamt val (lambda (x) (+ wt x)) 0)))
             (pure (make-hamt hash eqv?))
             req
             rest))

    ;; Collecting and normalizing the results as an alist

    (define (sum-probability req)
      (output-sum probability-ledger req))

    (define (collect-distribution req fold)
      (fmap (lambda (log)
              (let ((acc (writer-log-value log))
                    (total (writer-log-output log)))
                (fold (lambda (val wt lst)
                        (cons (cons (/ wt total)
                                    val)
                              lst))
                      '()
                      acc)))
            (sum-probability req)))

    (define (collect-distribution-sorted < req . rest)
      (collect-distribution (apply tabulate-bbtree < req rest)
                            bbtree-fold-right))

    (define (collect-distribution-hashed hash eqv? req . rest)
      (collect-distribution (apply tabulate-hamt hash eqv? req rest)
                            bbtree-fold-right))))
