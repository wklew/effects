(import (scheme base)
        (scheme write)
        (scheme case-lambda)
        (effects base)
        (effects lambda)
        (effects logic)
        (effects partial)
        (effects io)
        (effects syntax)
        (effects sharing)
        (srfi srfi-1))

(define (displayln x)
  (display x)
  (newline))

(define edb-people
  '(alice bob carol dave eve fred gwen hanna))

(define edb-relations
  '((carol . alice)
    (carol . bob)
    (alice . dave)
    (eve   . dave)
    (gwen  . fred)
    (hanna . gwen)))

(define edb-relations-prompt (make-prompt-tag 'edb-relations))

(define (run-edb req)
  (input-const edb-relations-prompt edb-relations req))

(define (run-stdout req)
  (output-yield 'stdout displayln req))

(define gen-person
  (list->choice edb-people))

(define gen-relation
  (list->choice edb-relations))

(define (%datalog req)
  (choice->list (run-shared (run-stdout (run-edb req)))))

(define-syntax datalog
  (syntax-rules ()
    ((_ (name ...) req ...)
     (%datalog
      (mlet ((name gen-person) ...)
        (mbegin req ...)
        (lift list (lift cons 'name name) ...))))))

(val/names (child desc)

  (def child
    (lam* (x y)
      (put 'stdout (lift list 'child? (var x) (var y)))
      (satisfy logic-failure
               equal?
               gen-relation
               (lift cons (var x) (var y)))
      (put 'stdout 'true)))

  (def desc
    (lam* (x y)
      (put 'stdout (lift list 'descendent-base? (var x) (var y)))
      (app (var child)
           (var x)
           (var y))))

  (def desc
    (app fix
         (lam* (rec x y)
           (put 'stdout (lift list 'descendant-rec? (var x) (var y)))
           (mlet ((z gen-person))
             (mbegin (app (var child)
                          z
                          (var y))
                     (app (var rec)
                          (var x)
                          z))))))

  (datalog (x)
    (app (var desc) 'carol x)))
