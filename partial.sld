;;; (effects partial) --- Exceptions

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides effects and handlers for modeling exceptions and
;;; partial computations (computations which may fail to return a value).

;;; Code:

(define-library (effects partial)

  (export throw
          exception-handler
          partial-handler
          finally
          catch
          assertion-failure?
          assert
          unsatisfied-failure?
          unsatisfied-failure-arguments
          satisfy)

  (import (scheme base)
          (scheme case-lambda)
          (effects base))

  (begin

    (define partial-prompt (make-prompt-tag 'partial))

    (define-record-type <exception>
      (make-exception tag arg)
      exception?
      (tag exception-tag)
      (arg exception-argument))

    (define throw
      (case-lambda
        ((tag)
         (throw tag unit))
        ((tag req)
         (bind req
           (lambda (arg)
             (let loop ()
               (request partial-prompt (make-exception tag arg)
                 (lambda (_)
                   (loop)))))))))

    (define (exception-handler proc)
      (make-handler partial-prompt
        (lambda (_ exc)
          (proc (exception-tag exc)
                (exception-argument exc)))))

    (define (partial-handler tag proc)
      (exception-handler
       (lambda (tag* arg)
         (if (eq? tag tag*)
             (proc arg)
             (throw tag* arg)))))

    (define (finally thunk req)
      (handle req
        `(,(exception-handler
            (lambda (tag arg)
              (bind (thunk)
                (lambda (_)
                  (throw tag arg))))))
        (lambda (x)
          (bind (thunk)
            (lambda (_)
              x)))))

    (define (catch tag req proc . rest)
      (apply handle
             req
             `(,(partial-handler tag proc))
             rest))

    (define-record-type <assertion-failure>
      (make-assertion-failure)
      assertion-failure?)

    (define (assert tag req)
      (bind req
        (lambda (x)
          (if x
              unit
              (throw tag (make-assertion-failure))))))

    (define-record-type <unsatisfied-failure>
      (make-unsatisfied-failure args)
      unsatisfied-failure?
      (args unsatisfied-failure-arguments))

    (define (satisfy tag pred . reqs)
      (bind (apply lift list reqs)
        (lambda (xs)
          (if (apply pred xs)
              xs
              (throw tag (make-unsatisfied-failure xs))))))))
