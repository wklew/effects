(define-library (effects handler)

  (export call/prompt
          handler-pass proto->handler handler-sum handler->proto)

  (import (scheme base)
          (scheme write)
          (effects base)
          (effects proto)
          (containers dictionary))

  (begin

    (define (call/prompt tag arg k)
      (call (cons tag arg)
        k))

    (define handler-pass dictionary-empty)
    (define proto->handler dictionary-single)
    (define handler-sum dictionary-merge)

    (define (handler->proto dict)
      (lambda (self super)
        (response
         (lambda (store x)
           ;; Pass the final pure value through each handler
           (dictionary-foldl
            (lambda (tag proto acc)
              ;; Modify self to insert the resulting state back into the store
              (define self*
                (proto (lambda (st req)
                         (self (dictionary-insert tag st store)
                               req))
                       super))
              (bind acc
                (lambda (x)
                  (self* (dictionary-lookup tag store)
                         x))))
            x
            dict))
         (lambda (store op k)
           (guard (e ((dictionary-lookup-error? e)
                      ;; If no handler is found for this prompt,
                      ;; reinstall self for a future handler
                      (call op
                        (lambda (x)
                          (self store (k x))))))
             ;; Lookup handler for this prompt, instantiate and run it
             (let* ((tag (car op))
                    (proto (dictionary-lookup tag dict)))
               ;; Modify self to insert the resulting state back into the store
               (define self*
                 (proto (lambda (st req)
                          (self (dictionary-insert tag st store)
                                req))
                        super))
               (self* (dictionary-lookup tag store)
                      (call (cdr op) k))))))))))
