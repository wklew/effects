;;; (effects derived-io) --- Derived input/output handlers

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides handlers for simulating "real-world" I/O using
;;; Scheme's port interface. Modelling I/O as an effect in combination with
;;; lazy sharing allows for smarter resource management: ports are
;;; automatically opened when first needed, and closed when no longer in use.

;;; Code:

(define-library (effects derived-io)

  (export stdout
          eof-failure
          read-input-from-file
          read-input-from-string
          write-output-to-file
          write-output-to-string)

  (import (scheme base)
          (scheme file)
          (effects base)
          (effects syntax)
          (effects io)
          (effects sharing)
          (effects partial))

  (begin

    (define stdout (make-prompt-tag 'stdout))

    (define (log x)
      (put stdout (pure x)))

    (define (do-acquire proc x)
      (abegin
        (log "Acquiring resource from <")
        (log x)
        (log ">\n")
        (mlet ((res (proc x)))
          (log "Acquired resource ")
          (log res)
          (log "\n")
          (pure res))))

    (define (do-release proc res)
      (abegin
        (log "Releasing resource ")
        (log res)
        (log "\n")
        (proc res)
        (log "Released")
        (log "\n")))

    (define eof-failure (make-prompt-tag 'eof))

    (define (do-receive proc res)
      (abegin
        (log "Receiving from resource ")
        (log res)
        (log "\n")
        (mlet ((x (proc res)))
          (log "Received ")
          (log x)
          (log "\n")
          (if (eof-object? x)
              (throw eof-failure)
              (pure x)))))

    (define (do-send proc x res)
      (abegin
        (log "Sending ")
        (log x)
        (log " to resource ")
        (log res)
        (log "\n")
        (proc x res)
        (log "Sent")
        (log "\n")))

    (define (release-port port)
      (do-release (lambda (port)
                    (pure (close-port port)))
                  port))

    (define (write-line str port)
      (write-string str port)
      (newline port))

    (define (read-input-from-file shared-tag io-tag file req)
      (define acquire
        (do-acquire (lambda (file)
                      (pure (open-input-file file)))
                    file))
      (define receive
        (mlet ((port (share shared-tag acquire)))
          (do-receive (lambda (port)
                        (pure (read-line port)))
                      port)))
      (finalize-shared shared-tag
                       release-port
                       (input-request io-tag receive req)))

    (define (read-input-from-string shared-tag io-tag str req)
      (define acquire
        (do-acquire (lambda (str)
                      (pure (open-input-string str)))
                    str))
      (define receive
        (mlet ((port (share shared-tag acquire)))
          (do-receive (lambda (port)
                        (pure (read-line port)))
                      port)))
      (finalize-shared shared-tag
                       release-port
                       (input-request io-tag receive req)))

    (define (write-output-to-file shared-tag io-tag file req)
      (define acquire
        (do-acquire (lambda (file)
                      (pure (open-output-file file)))
                    file))
      (define (send str)
        (mlet ((port (share shared-tag acquire)))
          (do-send (lambda (str port)
                     (pure (write-line str port)))
                   str
                   port)))
      (finalize-shared shared-tag
                       release-port
                       (output-request io-tag send req)))

    (define (write-output-to-string shared-tag io-tag req)
      (define acquire
        (do-acquire (lambda (_)
                      (pure (open-output-string)))
                    unit))
      (define (send str)
        (mlet ((port (share shared-tag acquire)))
          (do-send (lambda (str port)
                     (pure (write-line str port)))
                   str
                   port)))
      (define (release port)
        (let ((str (get-output-string port)))
          (abegin
            (release-port port)
            (pure str))))
      (finalize-shared shared-tag
                       release
                       (output-request send io-tag req)))))
