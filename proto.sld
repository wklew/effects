;;; (effects proto) --- Mixin-based handler prototypes

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;; See the paper: Cook and Palsberg, 1989,
;;; A Denotational Semantics of Inheritance and its Correctness

(define-library (effects proto)

  (export response defer mix pass instance)

  (import (scheme base)
          (effects base))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    (define (response pur imp)
      (lambda (dict req)
        (respond req
          (lambda (x)
            (pur dict x))
          (lambda (op k)
            (imp dict op k)))))

    ;; The standard mixin combinator for inheritance

    (define (%mix proto1 proto2)
      (lambda (self super)
        (proto1 self (proto2 self super))))

    ;; The identity mixin, which always defers to super

    (define (defer _self super) super)

    ;; Mixins form a monoid

    (define (mix . protos)
      (fold-right %mix defer protos))

    ;; The identity proto

    (define pass
      (response (lambda (_dict x) x)
                (lambda (_dict op k) (call op k))))

    ;; The fixed-point combinator for instantiating a proto

    (define (instance proto base)
      (define self
        (proto (lambda (dict req)
                 (self dict req))
               base))
      self)))
