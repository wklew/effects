;;; (effects parser) --- Monadic parsers

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library defines a monadic parsing interface, in the style of
;;; Haskell's Parsec library.

;;; Code:

(define-library (effects parser)

  (export parser-failure
          parse
          sat
          any-char
          char
          try
          unparse
          eof
          alt
          many
          chars
          lit
          any-chars
          any-string
          parse-list
          parse-string)

  (import (scheme base)
          (scheme write)
          (effects base)
          (effects functor)
          (effects syntax)
          (effects tuple)
          (effects io)
          (effects partial)
          (effects logic))

  (begin

    ;; Parsers return a nondeterministic set of parsed values, or answers. Each
    ;; answer belongs to a world in which the parser succeeded on the given input.
    ;; A set of zero answers means there was no possible way of parsing the input
    ;; string; a set of one or more answers means there was one or more ways of
    ;; doing so. The world corresponding to each answer contains a mutable
    ;; reference cell, or state, whose value is a generator representing the
    ;; remaining input not yet parsed in that world.

    (define stream (make-prompt-tag 'stream))

    (define parser-failure (make-prompt-tag 'parser))

    (define parse
      (get stream))

    (define (sat pred)
      (satisfy pred parser-failure parse))

    (define any-char
      (sat char?))

    (define (char c)
      (sat (lambda (x) (eqv? x c))))

    (define (try req)
      (catch parser-failure
        req
        (lambda (flr)
          (sequence
            (unparse (unsatisfied-failure-value flr))
            (fail)))))

    (define (unparse x)
      (put stream (pure x)))

    (define parser-label (make-prompt-tag 'parser))

    (define eof
      (label parser-label "End of input" (logic-invert (try any-char))))

    (define (alt reqs)
      (choose (map try reqs)))

    (define (many req)
      (let loop ((acc '()))
        (logic-cut (try req)
          (lambda (x)
            (loop (cons x acc)))
          (lambda ()
            (pure (reverse acc))))))

    (define (chars lst)
      (apply lift list (map char lst)))

    (define (lit str)
      (fmap list->string (chars (string->list str))))

    (define any-chars
      (many any-char))

    (define any-string
      (fmap list->string any-chars))

    (define (parse-list lst req)
      (update-list stream lst req))

    (define (parse-string str req)
      (parse-list (string->list str)
                  req))))
