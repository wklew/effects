(define-library (effects elist)

  (export elist-map elist-sequence)

  (import (scheme base)
          (effects base))

  (begin

    (define (elist-map f mxs)
      (let loop ((mxs mxs)
                 (ys '()))
        (bind mxs
          (lambda (xs)
            (if (null? xs)
                (reverse ys)
                (bind (f (car xs))
                  (lambda (y)
                    (loop (cdr xs)
                          (cons y ys)))))))))

    (define (elist-sequence mxs)
      (let loop ((mxs mxs)
                 (ys '()))
        (bind mxs
          (lambda (xs)
            (if (null? xs)
                (reverse ys)
                (bind (car xs)
                  (lambda (x)
                    (loop (cdr xs)
                          (cons x ys)))))))))))
