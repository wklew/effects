(define-library (effects session)

  (export await
          reply
          session-handler
          session->requester
          session->responder)

  (import (scheme base)
          (effects base)
          (effects tuple)
          (effects logic))

  (begin

    (define session-prompt (make-prompt-tag 'session))

    (define-record-type <session-event>
      (make-session-event lbl await?)
      session-event?
      (lbl session-event-label)
      (await? session-event-await?))

    (define (await req)
      (bind req
        (lambda (x)
          (request session-prompt (make-session-event x #t)))))

    (define (reply req)
      (bind req
        (lambda (x)
          (request session-prompt (make-session-event x #f)))))

    (define (session-handler proc)
      (make-handler session-prompt
        (lambda (cont evt)
          (proc cont
                (session-event-label evt)
                (session-event-await? evt)))))

    (define (receive in-chan decode fmt)
      (get in-chan
        (lambda (x)
          (decode fmt x))))

    (define (transmit out-chan req)
      (bind req
        (lambda (p)
          (put out-chan (pure (car p))
            (lambda ()
              (pure (cdr p)))))))

    (define (session->requester out-chan encode in-chan decode req)
      (let loop ((req req))
        (handle req
          `(,(session-handler
              (lambda (cont lbl await?)
                (bind (if await?
                          (transmit out-chan (encode lbl))
                          (accept in-chan decode lbl))
                  (lambda (x)
                    (loop (cont x))))))))))

    (define (session->responder in-chan decode out-chan encode req)
      (handle req
        `(,(session-handler
            (lambda (cont lbl await?)
              (bind (if await?
                        (accept in-chan decode lbl)
                        (transmit out-chan (encode lbl)))
                (lambda (x)
                  (loop (cont x)))))))))))
