(use-modules (srfi srfi-9 gnu)
             (ice-9 format))

(set-record-type-printer!
 (@@ (effects base) <operation>)
 (lambda (op port)
   (format port
           "(op ~a ~a)"
           ((@@ (effects base) operation-tag) op)
           ((@@ (effects base) operation-argument) op))))

(set-record-type-printer!
 (@@ (effects base) <impure>)
 (lambda (imp port)
   (format port
           "(impure ~a)"
           ((@@ (effects base) impure-operation) imp))))

(set-record-type-printer!
 (@@ (effects lambda) <closure>)
 (lambda (clos port)
   (format port
           "(closure ~a)"
           ((@@ (effects lambda) closure-name) clos))))

(set-record-type-printer!
 (@@ (effects io) <lookup>)
 (lambda (lkp port)
   (format port
           "(lookup ~a)"
           ((@@ (effects io) lookup-tag) lkp))))

(set-record-type-printer!
 (@@ (effects tuple) <annotation>)
 (lambda (ann port)
   (format port
           "(annotation ~a ~a)"
           ((@@ (effects tuple) annotation-prompt) ann)
           ((@@ (effects tuple) annotation-value) ann))))

(set-record-type-printer!
 (@@ (effects logic) <choice>)
 (lambda (_ port)
   (format port "(choice)")))
