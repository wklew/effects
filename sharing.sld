;;; (effects sharing) --- Lazy sharing of effects

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library implements lazy sharing of effects.
;;; Shared effects are evaluated using a call-by-need strategy:
;;; they are evaluated at most once, on first evaluation,
;;; and the resulting value is then memoized.

;;; Code:

(define-library (effects sharing)

  (export cache run-cached share run-shared)

  (import (scheme base)
          (effects base)
          (effects io)
          (effects syntax)
          (containers dictionary))

  (begin

    (define cache-prompt (make-prompt-tag 'cache))

    (define (cache key req)
      (get cache-prompt
        (lambda (dict)
          (guard (e ((dictionary-lookup-error? e)
                     (mlet ((x req))
                       (put cache-prompt (dictionary-insert key x dict))
                       x)))
            (dictionary-lookup key dict)))))

    (define (run-cached req)
      (bind (update-state cache-prompt (dictionary-empty) req)
        state-result-value))

    (define share-prompt (make-prompt-tag 'share))

    (define (share req)
      (get share-prompt
        (lambda (n)
          ;; we explicitly wrap the effect in `pure`
          ;; to manipulate it as data, without handling it
          (pure (cache n req)))))

    (define (run-shared req)
      (input-iota share-prompt (run-cached req)))))
