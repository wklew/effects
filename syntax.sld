;;; (effects syntax) --- Monadic and applicative syntax

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of disteff.
;;;
;;; disteff is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; disteff is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with disteff.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides syntactic extensions for working with
;;; applicative and monadic combinations of effects.

;;; Code:

(define-library (effects syntax)

  (export mbegin mlet mwhen mdo)

  (import (scheme base)
          (effects base))

  (begin

    ;; Monadic binding syntax.
    ;; These follow the scheme convention of sequencing body expressions.

    ;; Monadic substitution, binding variables in series

    (define-syntax mbegin
      (syntax-rules ()
        ((_)
         unit)
        ((_ req)
         req)
        ((_ req req* ...)
         (bind req (lambda (_) (mbegin req* ...))))))

    (define-syntax mlet
      (syntax-rules ()
        ((_ () body ...)
         (mbegin body ...))
        ((_ ((x expr) . bindings) body ...)
         (bind expr
           (lambda (x)
             (mlet bindings body ...))))))

    ;; Monadic partial conditional

    (define-syntax mwhen
      (syntax-rules ()
        ((_ test body ...)
         (if test
             (sequence body ...)
             unit))))

    ;; Monadic do notation, from Haskell

    (define-syntax mdo
      (syntax-rules (<-)
        ((_)
         unit)
        ((_ req)
         req)
        ((_ (x <- req) req* ...)
         (bind req
           (lambda (x)
             (mdo req* ...))))
        ((_ req req* ...)
         (bind req (lambda (_) (mdo req* ...))))))))
