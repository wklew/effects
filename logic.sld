;;; (effects logic) --- Nondeterministic logic

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library extends the partial effect with effects and handlers for
;;; modeling nondeterminism. This allows for Prolog-style logic programming.

;;; Code:

(define-library (effects logic)

  (export branch
          choice-handler
          logic-failure
          fail
          list->choice
          choice
          stream-null?
          stream-pair?
          stream-car
          stream-cdr
          split
          once
          logic-take
          logic-drop
          logic-invert
          logic-cut
          fair-branch
          fair-bind
          choice-fold
          choice->list
          choice->sequence
          choice-every)

  (import (scheme base)
          (scheme case-lambda)
          (effects base)
          (effects partial)
          (containers sequence))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chicken (import (srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    (define choice-prompt (make-prompt-tag 'choice))

    (define-record-type <choice>
      (make-choice)
      choice?)

    (define (branch* left right)
      (request choice-prompt (make-choice)
        (lambda (?)
          (if ? (left) (right)))))

    (define-syntax branch
      (syntax-rules ()
        ((_ left right)
         (branch* (lambda () left)
                  (lambda () right)))))

    (define (choice-handler proc)
      (make-handler choice-prompt
        (lambda (cont _)
          (proc (cont #t)
                (cont #f)))))

    (define logic-failure (make-prompt-tag 'logic))

    (define (fail)
      (throw logic-failure unit))

    (define (list->choice lst)
      (fold (lambda (req acc)
              (branch acc req))
            (fail)
            lst))

    (define (choice . reqs)
      (list->choice reqs))

    (define-record-type <stream-null>
      (stream-null)
      stream-null?)

    (define-record-type <stream-pair>
      (stream-cons val req)
      stream-pair?
      (val stream-car)
      (req stream-cdr))

    (define (split req)
      (let loop ((acc '())
                 (req req))
        (handle req
          `(,(choice-handler
              (lambda (left right)
                (loop (cons right acc)
                      left)))
            ,(partial-handler
              logic-failure
              (lambda (_)
                (if (null? acc)
                    (stream-null)
                    (loop (cdr acc)
                          (car acc))))))
          (lambda (x)
            (stream-cons x (list->choice acc))))))

    (define (iterate1 join alt req)
      (bind (split req)
        (lambda (strm)
          (if (stream-null? strm)
              (alt)
              (join (stream-car strm)
                    (stream-cdr strm))))))

    (define (once req)
      (iterate1 (lambda (x _) x)
                fail
                req))

    (define (logic-take n req)
      (let loop ((acc (fail))
                 (n n)
                 (req req))
        (if (= n 0)
            acc
            (iterate1 (lambda (x req)
                        (loop (branch acc x)
                              (- n 1)
                              req))
                      (lambda () acc)
                      req))))

    (define (logic-drop n req)
      (let loop ((n n)
                 (req req))
        (if (= n 0)
            req
            (iterate1 (lambda (_ req)
                        (loop (- n 1)
                              req))
                      (lambda () (fail))
                      req))))

    (define (logic-invert req)
      (iterate1 (lambda _ (fail))
                unit
                req))

    (define (logic-cut req cont alt)
      (iterate1 (lambda (x req)
                  (branch (cont x)
                          (bind req cont)))
                alt
                req))

    (define (fair-branch left right)
      (iterate1 (lambda (x req)
                  (branch x
                          (fair-branch right req)))
                (lambda () right)
                left))

    (define (fair-bind req cont)
      (iterate1 (lambda (x req)
                  (fair-branch (cont x)
                               (fair-bind req cont)))
                fail
                req))

    (define (choice-fold insert base req)
      (let loop ((acc base)
                 (req req))
        (iterate1 (lambda (x req)
                    (loop (insert x acc)
                          req))
                  (lambda () acc)
                  req)))

    (define (choice->list req)
      (bind (choice-fold (lambda (x req)
                           (bind req
                             (lambda (lst)
                               (cons x lst))))
                         '()
                         req)
        reverse))

    (define (choice->sequence req)
      (let loop ((acc (sequence-empty))
                 (req req))
        (handle req
          `(,(choice-handler
              (lambda (left right)
                (bind (choice->sequence left)
                  (lambda (acc*)
                    (loop (sequence-append acc acc*)
                          right)))))
            ,(partial-handler
              logic-failure
              (lambda (_)
                acc)))
          (lambda (x)
            (sequence-snoc x acc)))))

    (define (choice-every req)
      (let loop ((acc #t)
                 (req req))
        (if (not acc)
            (fail)
            (handle req
              `(,(choice-handler
                  (lambda (left right)
                    (bind (choice-every left)
                      (lambda (acc*)
                        (loop (and acc acc*)
                              right)))))
                ,(partial-handler
                  logic-failure
                  (lambda (_)
                    (assert logic-failure acc))))
              (lambda (x)
                (assert logic-failure (and acc x)))))))))
