(import (scheme base)
        (scheme write)
        (effects base)
        (effects functor)
        (effects syntax)
        (effects io)
        (effects partial)
        (effects logic)
        (effects tuple)
        (effects parser))

(define s1 "example")

(display (parse-string s1 (lit "example")))
(newline)

(display (once (parse-string s1 any-string)))
(newline)

(define s2 "example.com")

(display
 (once
  (parse-string s2
    (lift string-append
          (lit "example")
          (lit ".com")))))
(newline)

;; Applicative let binds its arguments in "parallel" (actually in reverse)

(display
 (once
  (parse-string s2
    (alet ((y (lit ".com"))
           (x (lit "example")))
      (string-append x y)))))
(newline)

;; More complex example: modular URI parser

(define-record-type <uri>
  (make-uri sec? doms file)
  uri?
  (sec? uri-secure?)
  (doms uri-domains)
  (file uri-filename))

(define scheme
  (mdo
    (lit "http")
    (secure? <- (branch (try (sequence (char #\s) (pure #t)))
                        (pure #f)))
    (lit "://")
    (pure secure?)))

(define (separator? x)
  (or (eof-object? x)
      (eqv? x #\/)
      (eqv? x #\.)))

(define domain-char
  (sat (lambda (x) (not (separator? x)))))

(define domain
  (fmap list->string (many domain-char)))

(define subdomain
  (sequence (char #\.) domain))

(define domains
  (lift cons domain (many subdomain)))

(define file
  (sequence (char #\/) any-string))

(define uri
  (lift make-uri scheme domains file))

(define s3 "https://www.example.com/index.php")

(display (once (parse-string s3 uri)))
(newline)
