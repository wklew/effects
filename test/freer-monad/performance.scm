;;; performance.scm --- Testing performance of reified continuation arrows

;;; Ordinarily, left-associated binds perform quadratically worse than
;;; right-associated binds, meaning that a continuation composed of
;;; a long string of left-associated binds suffers a performance hit.

;;; By reifying the same sequence of continuations onto a data structure
;;; designed to represent sequences efficiently, this penalty becomes
;;; logarithmic rather than quadratic in the number of composed continuations.

(import (scheme base)
        (scheme write)
        (effects base)
        (effects io))

(cond-expand
 (chibi (import (srfi 1)))
 (guile (import (srfi srfi-1))))

;;; Kleisli composition

(define (>>> f g)
  (lambda (x)
    (bind (f x) g)))

(define (add-get x)
  (get 0
    (lambda (i)
      (+ i x))))

;;; Unlike in Haskell, we build the list strictly.
;;; This doesn't seem to affect performance, however;
;;; a hand-built iterative loop seems to perform worse, if anything.

(define (product n)
  ((fold >>> identity (make-list n add-get))
   0))

;;; Using function composition to represent a chain of continuations,
;;; the following would take a very long time, or run out of memory,
;;; even though it is merely multiplying 1,000,000 by 5.
;;; Using reified continuations we recover a good bit of speed and memory.

(display (input-const 0 5 (product 1000000)))
(newline)

;;; Unfortunately, adding another handler negates this benefit.
;;; Even though the output handler never receives an output effect,
;;; its presence results in a linear degradation in performance.

(input-const 0 5 (output-last 1 (product 100000)))
