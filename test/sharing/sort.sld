(define-library (effects test sort)

  (export random-lists
          test-sort-strict
          test-sort-lazy)

  (import (scheme base)
          (scheme write)
          (effects base)
          (effects syntax)
          (effects io)
          (effects logic)
          (effects sharing)
          (effects elist))

  (cond-expand
   (guile (import (srfi srfi-1)
                  (srfi srfi-19)
                  (srfi srfi-27))
          (begin
            (define (current-seconds)
              (time-second (current-time)))))
   (chibi (import (srfi 1)
                  (srfi 27)
                  (chibi time))))

  (begin

    (define (dec n) (- n 1))

    (define (random-list range len)
      (unfold zero? (lambda (_) (random-integer range)) dec len))

    (define (random-lists range len count)
      (unfold zero? (lambda (_) (random-list range len)) dec count))

    (define (run-with-timer thunk)
      (let* ((t1 (current-seconds))
             (res (thunk))
             (t2 (current-seconds)))
        (list (- t2 t1) res)))

    (define (displayln x)
      (display x)
      (newline))

    (define (test-sort sort ls)
      (displayln "unsorted:")
      (for-each displayln ls)
      (displayln "sorted:")
      (for-each displayln
                (map (lambda (l)
                       (run-with-timer (lambda () (sort l))))
                     ls)))

    ;; Extremely inefficient strict sorting algorithm

    (define (sorted? lst)
      (or (null? lst)
          (let loop ((x (car lst))
                     (lst (cdr lst))
                     (acc #t))
            (if (null? lst)
                acc
                (let ((y (car lst))
                      (lst (cdr lst)))
                  ;; (display x)
                  ;; (display "<=")
                  ;; (display y)
                  ;; (newline)
                  (loop y lst (and acc (<= x y))))))))

    (define (insert x lst)
      (let loop ((lst lst))
        (choice (cons x lst)
                (if (null? lst)
                    (fail)
                    (let ((x (car lst))
                          (lst (cdr lst)))
                      (bind (loop lst)
                        (lambda (acc)
                          (cons x acc))))))))

    (define (permute lst)
      (let loop ((lst lst)
                 (req '()))
        (if (null? lst)
            req
            (loop (cdr lst)
                  (bind req
                    (lambda (acc)
                      (insert (car lst) acc)))))))

    (define (sort lst)
      (bind (permute lst)
        (lambda (lst)
          (if (sorted? lst)
              lst
              (fail)))))

    (define (run-sort-strict l)
      (once (sort l)))

    (define (test-sort-strict ls)
      (test-sort run-sort-strict ls))

    ;; More efficient version: rather than check sortedness strictly,
    ;; we assert the sortedness of each pair of elements incrementally

    (define (elist-sorted ml)
      (mlet ((l ml))
        (let loop ((l l))
          (mwhen (pair? l)
            (mlet ((xs (cdr l)))
              (mwhen (pair? xs)
                (mlet ((x (car l))
                       (y (car xs)))
                  (if (> x y)
                      (fail)
                      (loop (cons y (cdr xs)))))))))))

    (define (elist-sorted? ml)
      (mlet ((l ml))
        (let loop ((l l))
          (if (null? l)
              #t
              (mlet ((xs (cdr l)))
                (if (null? xs)
                    #t
                    (mlet ((x (car l))
                           (y (car xs)))
                      (if (> x y)
                          #f
                          (loop (cons y (cdr xs)))))))))))

    (define (elist-insert mx mxs)
      (choice (cons mx mxs)
              (mlet ((xs mxs))
                (if (null? xs)
                    (fail)
                    (cons (car xs)
                          (elist-insert mx (cdr xs)))))))

    (define (elist-permute mxs)
      (let loop ((mxs mxs)
                 (mys '()))
        (mlet ((xs mxs))
          (if (null? xs)
              mys
              (loop (cdr xs)
                    (elist-insert (car xs) mys))))))

    (define (elist-share mxs)
      (elist-map share mxs))

    (define (elist-sort mxs)
      (mlet ((mys (elist-share (elist-permute mxs))))
        (elist-sorted mys)
        mys))

    (define (run-sort-lazy l)
      (run-shared (once (elist-sequence (elist-sort l)))))

    (define (test-sort-lazy ls)
      (test-sort run-sort-lazy ls))))
