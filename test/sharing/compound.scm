(import (scheme base)
        (effects base)
        (effects syntax)
        (effects sharing)
        (effects logic))

(cond-expand
 (chibi (import (chibi test)))
 (guile (import (srfi srfi-64))
        (define (test name x y)
          (test-equal name x y))))

;; Evaluate the same request twice and return the results as a pair

(define (duplicate req)
  (lift cons req req))

;; Evaluate two different requests and return the results as a vector

(define (mix req1 req2)
  (lift vector req1 req2))

;; Nondeterministic symbols

(define a?b (choice 'a 'b))
(define x?y (choice 'x 'y))

;; Duplicating a strict mixture and then collecting the results is equivalent
;; to enumerating all possible combinations independently

(test "strict"

      '((#(a x) . #(a x))
        (#(a x) . #(a y))
        (#(a x) . #(b x))
        (#(a x) . #(b y))
        (#(a y) . #(a x))
        (#(a y) . #(a y))
        (#(a y) . #(b x))
        (#(a y) . #(b y))
        (#(b x) . #(a x))
        (#(b x) . #(a y))
        (#(b x) . #(b x))
        (#(b x) . #(b y))
        (#(b y) . #(a x))
        (#(b y) . #(a y))
        (#(b y) . #(b x))
        (#(b y) . #(b y)))

      (choice->list (duplicate (mix a?b x?y))))

;; Duplicating a cached mixture yields all possible pairs of equal mixtures

(test "cached-outer"

      '((#(a x) . #(a x))
        (#(a y) . #(a y))
        (#(b x) . #(b x))
        (#(b y) . #(b y)))

      (choice->list
       (run-cached (duplicate (cache 0 (mix a?b x?y))))))

;; The elements of the mixture can also be cached to yield the same results

(test "cached-inner"

      '((#(a x) . #(a x))
        (#(a y) . #(a y))
        (#(b x) . #(b x))
        (#(b y) . #(b y)))

      (choice->list
       (run-cached (duplicate (mix (cache 0 a?b)
                                (cache 1 x?y))))))

;;; Conflating memory locations results in one overwriting the other

(test "conflated"

      '((#(a a) . #(a a))
        (#(b b) . #(b b)))

      (choice->list
       (run-cached (duplicate (mix (cache 0 a?b)
                                (cache 0 x?y))))))

;;; The sharing effect automates the allocation of cache locations,
;;; preventing errors such as the one above

(test "shared"

      '((#(a x) . #(a x))
        (#(a y) . #(a y))
        (#(b x) . #(b x))
        (#(b y) . #(b y)))

      (choice->list
       (run-shared
        (mlet ((a?b* (share a?b))
               (x?y* (share x?y)))
          (duplicate
           (mix a?b* x?y*))))))
