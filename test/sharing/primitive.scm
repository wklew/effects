(import (scheme base)
        (effects base)
        (effects sharing)
        (effects io)
        (effects logic)
        (effects syntax))

(cond-expand
 (chibi (import (srfi 69) (chibi test)))
 (guile (import (srfi srfi-69) (srfi srfi-64))
        (define (test name x y)
          (test-equal name x y))))

(define stdout (make-prompt-tag 'stdout))

(define coin
  (sequence
    (put stdout "f")
    (choice #t #f)))

(define (duplicate req)
  (sequence
    (put stdout "d")
    (lift cons req req)))

(define dup-coin
  (duplicate coin))

(define dup-coin-bind
  (mlet ((x coin))
    (duplicate x)))

(define dup-coin-share
  (mlet ((x* (share coin)))
    (duplicate x*)))

(define (observe req)
  (output-string stdout (choice->list req)))

(mlet ((log (observe dup-coin)))
  (test "let binding (value)"
        ;; Outcomes of two independent coin flips
        '((#t . #t) (#t . #f) (#f . #t) (#f . #f))
        (writer-log-value log))
  (test "let binding (output)"
        ;; Flipped three times, inside duplicate
        "dfff"
        (writer-log-output log)))

(mlet ((log (observe dup-coin-bind)))
  (test "monadic binding (value)"
        ;; Duplicated outcomes of a single coin flip
        '((#t . #t) (#f . #f))
        (writer-log-value log))
  (test "monadic binding (output)"
        ;; Flipped once, before duplicate; duplicated twice
        "fdd"
        (writer-log-output log)))

(mlet ((log (observe (run-shared dup-coin-share))))
  (test "sharing (value)"
        ;; Same result as monadic bind
        '((#t . #t) (#f . #f))
        (writer-log-value log))
  (test "sharing (output)"
        ;; Flipped once, *inside* duplicate; duplicated once
        "df"
        (writer-log-output log)))
