(import (scheme base)
        (effects base)
        (effects proto)
        (effects handler)
        (containers dictionary))

;;; We'll demo with reader and writer effects

(define reader-prompt 0)
(define writer-prompt 1)

(define (get k)
  (call/prompt reader-prompt unit k))

(define (put x th)
  (call/prompt writer-prompt x (lambda (_) (th))))

;;; Some test terms

(define get+get
  (get (lambda (x) (get (lambda (y) (+ x y))))))

(define put_put
  (put 10 (lambda () (put 20 (lambda () 'done)))))

(define get_put+get_put
  (get (lambda (x) (put x (lambda () (get (lambda (y) (put y (lambda () (+ x y))))))))))

;;; Example class definitions for reader and writer handlers:
;;; The reader reads from a stream of values;
;;; The writer accumulates a monoidal output value

(define $reader-list/any
  (lambda (self super)
    (response
     (lambda (lst x)
       (super lst `(reader-result ,x ,lst)))
     (lambda (lst arg k)
       (self (cdr lst) (k (car lst)))))))

(define ($writer-fold/any combine)
  (lambda (self super)
    (response
     (lambda (acc x)
       (super acc `(writer-result ,x ,acc)))
     (lambda (acc arg k)
       (self (combine acc arg) (k #f))))))

(define handler-reader-list
  (proto->handler reader-prompt $reader-list/any))

(define (handler-writer-fold combine)
  (proto->handler writer-prompt ($writer-fold/any combine)))

;;; Bespoke reader/writer which uses a dictionary for efficiency

(define (handler-reader-list/writer-fold combine)
  (handler-sum handler-reader-list (handler-writer-fold combine)))

;;; Specializing the handlers further

(define handler-writer-sum (handler-writer-fold +))
(define handler-writer-product (handler-writer-fold *))
(define handler-reader-list/writer-product (handler-reader-list/writer-fold *))

;;; Instantiating them once and for all

(define reader-list (instance (handler->proto handler-reader-list) pass))
(define writer-sum (instance (handler->proto handler-writer-sum) pass))
(define writer-product (instance (handler->proto handler-writer-product) pass))
(define reader-list/writer-product
  (instance (handler->proto handler-reader-list/writer-product)
            pass))

(define (run-reader-list lst req)
  (reader-list (dictionary-single reader-prompt lst)
               req))

(define (run-writer-sum req)
  (writer-sum (dictionary-single writer-prompt 0)
               req))

(define (run-writer-product req)
  (writer-product (dictionary-single writer-prompt 1)
                  req))

(define (run-reader-list/writer-product lst req)
  (reader-list/writer-product
   (alist->dictionary `((,reader-prompt . ,lst)
                        (,writer-prompt . 1)))
   req))

;;; Interpreting the effects

(run-reader-list '(3 4) get+get)
;; => (reader-result 7 ())

(run-writer-product put_put)
;; => (writer-result done 200)

(run-reader-list '(3 4) (run-writer-product get_put+get_put))
;; => (reader-result (writer-result 7 12) ())

(run-writer-product (run-reader-list '(3 4) get_put+get_put))
;; => (writer-result (reader-result 7 ()) 12)

;;; The bespoke handler

(run-reader-list/writer-product '(3 4) get+get)
;; => (writer-result (reader-result 7 ()) 1)

(run-reader-list/writer-product '(3 4) put_put)
;; => (writer-result (reader-result done (3 4)) 200)

(run-reader-list/writer-product '(3 4) get_put+get_put)
;; => (writer-result (reader-result 7 ()) 12)
