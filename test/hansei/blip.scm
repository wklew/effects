(import (scheme base)
        (effects base)
        (effects functor)
        (effects syntax)
        (effects partial)               ; exceptions to model failure
        (effects logic)                 ; logic programming core library
        (effects probability)           ; probabilistic inference
        (effects sharing)               ; lazy sharing of simulation state
        (effects io)                    ; tracking simulation state
        (effects lambda)                     ; higher-order request parameters
        )

(cond-expand
 (guile (import (srfi srfi-1)))
 (chibi (import (srfi 1))))

;;; Geometry interface

(define width 10)
(define height 10)

(define (x0 idx) (uniform width))
(define (y0 idx) (uniform height))
(define (direction0 idx) (uniform 4))

(define x-derivs
  (vector-map
   alist->distribution
   #(((.1 . -1) (.8 . +0) (.1 . +1))    ; north
     ((.2 . +0) (.7 . +1) (.1 . +2))    ; east
     ((.1 . -1) (.8 . +0) (.1 . +1))    ; south
     ((.2 . +0) (.7 . -1) (.1 . +2))))) ; west

(define y-derivs
  (vector-map
   alist->distribution
   #(((.2 . +0) (.7 . +1) (.1 . +2))
     ((.1 . -1) (.8 . +0) (.1 . +1))
     ((.2 . +0) (.7 . -1) (.1 . +2))
     ((.1 . -1) (.8 . +0) (.1 . +1)))))

(define direction-turns
  (vector-map
   alist->distribution
   #(((.2 . 3) (.6 . 0) (.2 . 1))
     ((.2 . 0) (.6 . 1) (.2 . 2))
     ((.2 . 1) (.6 . 2) (.2 . 3))
     ((.2 . 2) (.6 . 3) (.2 . 0)))))

(define (x-deriv idx time dir)
  (vector-ref x-derivs dir))

(define (y-deriv idx time dir)
  (vector-ref y-derivs dir))

(define (direction-turn idx time dir)
  (vector-ref direction-turns dir))

(define (x-bound x)
  (if (and (>= x 0)
           (< x width))
      (pure x)
      (fail)))

(define (y-bound y)
  (if (and (>= y 0)
           (< y height))
      (pure y)
      (fail)))

(define (make-position x y) (vector x y))
(define (position-x vec) (vector-ref vec 0))
(define (position-y vec) (vector-ref vec 1))

(define (position0 idx)
  (make-position (x0 idx)
                 (y0 idx)))

(define (position-traverse proc pos)
  (alet ((x (proc (position-x pos)))
         (y (proc (position-y pos))))
    (make-position x y)))

(define (position-later pos)
  (make-position (later (position-x pos))
                 (later (position-y pos))))

(define (position-now pos)
  (make-position (now (position-x pos))
                 (now (position-y pos))))

(define (position-unify pos* pos)
  (mlet ((x (now (position-x pos*))))
    (if (not (= x (position-x pos)))
        (pure #f)
        (alet ((y (now (position-y pos*))))
          (= y (position-y pos))))))

(define-record-type <plane>
  (make-plane idx pos dir)
  plane?
  (idx plane-index)
  (pos plane-position)
  (dir plane-direction))

(define (plane0 idx)
  (make-plane idx
              (position0 idx)
              (direction0 idx)))

(define (plane-later plane)
  (make-plane (plane-index plane)
              (position-later (plane-position plane))
              (later (plane-direction plane))))

(define (plane-now plane*)
  (make-plane (plane-index plane)
              (position-now (plane-position plane))
              (now (plane-direction plane))))

(define (plane-fly time plane)
  (let ((idx (pure (plane-index plane)))
        (pos (plane-position plane)))
    (mlet ((dir* (plane-direction plane))
           (x* (position-x pos))
           (y* (position-y pos)))
      (make-plane idx
                  (make-position
                   (later
                    (mlet ((dir dir*)
                           (x x*)
                           (dx (x-deriv idx time dir)))
                      (x-bound (+ x dx))))
                   (later
                    (mlet ((dir dir*)
                           (y y*)
                           (dy (y-deriv idx time dir)))
                      (y-bound (+ y dy)))))
                  (later
                    (mlet ((dir dir*))
                      (direction-turn idx time dir)))))))

;;; Some initial tests

(define (abj p q)
  (and p (not q)))

(define (plane-3:3 plane)
  (position-unify (plane-position plane)
                  (make-position 3 3)))

(define test1
  (collect-distribution-sorted
   abj
   (eventual (plane-3:3 (plane-later (plane0 1))))))

;; ((1/100 . #t) (99/100 . #f))

;;; The simulation proper

(define annihilate-param (make-prompt-tag 'annihilate))
(define create-param (make-prompt-tag 'create))

(define counter-state (make-prompt-tag 'counter))
(define planes-state (make-prompt-tag 'planes))

(define do-annihilate
  (put planes-state
    (app (get annihilate-param)
         (get planes-state))))

(define (do-fly time)
  (put planes-state
    (alet ((planes (get planes-state)))
      (map (lambda (plane)
             (plane-fly time plane))
           planes))))

(define (spawn cnt planes idx)
  (let loop ((planes planes)
             (idx idx))
    (if (< idx cnt)
        (loop (cons (plane0 idx)
                    planes)
              (+ idx 1))
        planes)))

(define do-create
  (mlet ((cnt (get counter-state))
         (cnt* (alet ((crt (get create-param)))
                 (+ cnt crt))))
    (put counter-state (pure cnt*))
    (put planes-state
      (alet ((planes (get planes-state)))
        (spawn cnt* planes cnt)))))

(define (step time check)
  (mdo
    do-annihilate
    (do-fly time)
    do-create
    (planes <- (get planes-state))
    (check planes time)))

(define-record-type <blip>
  (make-evidence val holds?)
  evidence?
  (val evidence-value)
  (holds? evidence-holds?))

(define (observe-ideal pos evs)
  (let loop ((acc '())
             (evs evs))
    (if (null? evs)
        (fail)
        (let ((ev (car evs))
              (evs (cdr evs)))
          (branch
           (alet ((pos* (position-unify pos (evidence-value ev))))
             (fold cons
                   (cons (make-evidence pos* #t)
                         evs)
                   acc))
           (loop (cons ev acc)
                 (cdr evs)))))))

(define (blips-ideal blips planes)
  (let* ((evs (map (lambda (pos)
                     (make-evidence pos #f))
                   blips))
         (evs (fold (lambda (evs req)
                      (mlet ((plane req))
                        (observe-ideal (plane-position plane)
                                       evs)))
                    evs
                    planes)))
    (sequence
      (map (lambda (ev)
             (assert logic-failure (evidence-holds? ev)))
           evs))))

(define (create-const count req)
  (input-value create-param count req))

(define (create-null req)
  (create-const 0 req))

(define (create-geom count req)
  (input-request create-param (geometric .85 count) req))

(define (annihilate-null req)
  (input-request annihilate-param id req))

(define (run-counter req)
  (update-state counter-state 0 req))

(define (run-planes req)
  (update-state planes-state '() req))
