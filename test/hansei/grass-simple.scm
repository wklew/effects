(import (scheme base)
        (scheme write)
        (effects base)
        (effects functor)
        (effects syntax)
        (effects logic)
        (effects probability)
        (effects sharing)
        (effects io))

;; Distributions

(define (bern weight)
  (flip weight (pure #t) (pure #f)))

;; Execution trace

(define stdout (make-prompt-tag 'stdout))

(define (trace str)
  (put stdout (pure str)))

;; Inference

(define (write-line str)
  (write-string str)
  (newline))

(define (abj p q)
  (and p (not q)))

(define (odds req)
  (output-yield
   stdout
   write-line
   (collect-distribution-sorted
    abj
    req)))

;; Model components

(define obs-rain?
  (sequence
    (trace "random rain")
    (bern 1/2)))

(odds obs-rain?)

(define obs-sprinkler?
  (sequence
    (trace "random sprinkler")
    (bern 1/7)))

(odds obs-sprinkler?)

(define (obs-wet-grass? obs-rain? obs-sprinkler?)
  (sequence
    (trace "rain or sprinkler wet grass")
    (alet ((rain? obs-rain?)
           (sprinkler? obs-sprinkler?))
      (or rain? sprinkler?))))

(odds (obs-wet-grass? obs-rain? obs-sprinkler?))

(define grass-model-strict
  (mlet ((rain? obs-rain?)
         (sprinkler? obs-sprinkler?)
         (wet-grass? (obs-wet-grass?
                      (pure rain?)
                      (pure sprinkler?))))
    (if wet-grass?
        (pure rain?)
        (fail))))

(display (odds grass-model-strict))
(newline)

;; random rain
;; random sprinkler
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random sprinkler
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass

(define grass-model-lazy
  (mlet ((pr-rain? (later obs-rain?))
         (pr-sprinkler? (later obs-sprinkler?))
         (wet-grass? (obs-wet-grass? pr-rain? pr-sprinkler?)))
    (if wet-grass?
        pr-rain?
        (fail))))

(begin
  (display (odds (now grass-model-lazy)))
  (newline))

;; rain or sprinkler wet grass
;; random sprinkler
;; random rain
;; random rain
