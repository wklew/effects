;;; lazy.scm --- Testing laziness with probability distributions

;;; This is adapted directly from Oleg's hansei source code at:
;;; http://okmij.org/ftp/kakuritu/slazy.ml

(import (scheme base)
        (scheme lazy)                   ; built-in Scheme laziness
        (effects base)                  ; effects core
        (effects functor)               ; functor lifting
        (effects syntax)                ; syntactic extensions
        (effects logic)                 ; nondeterministic choice
        (effects probability)           ; probability measurements
        (effects sharing)               ; lazy sharing of effect results
        (effects io)                    ; I/O handling
        )

(cond-expand
 (chibi (import (srfi 69) (chibi test)))
 (guile (import (srfi srfi-69) (srfi srfi-64))
        (define (test name x y)
          (test-equal name x y))))

(define (collect req)
  (collect-distribution-sorted string<? req))

(define stdout (make-prompt-tag 'stdout))

(define coin
  (flip 1/2 (pure "a") (pure "b")))

(define (tails? x)
  (equal? x "b"))

(define (expensive x)
  (sequence
    (put stdout (pure "$"))
    (pure x)))

(define (twiddle u)
  (bind (flip 1/2
          (pure (string-append u "x"))
          (pure (string-append u "y")))
    expensive))

(define strict-test
  (mlet ((u coin)
         (x (twiddle u)))
    (put stdout (pure u))
    (if (tails? u)
        (pure "*")
        (alet ((y coin))
          (string-append x x y)))))

(define thunk-test
  (mlet ((u coin)
         (x (pure (lambda () (twiddle u)))))
    (put stdout (pure u))
    (if (tails? u)
        (pure "*")
        (alet ((x1 (x))
               (x2 (x))
               (y coin))
          (string-append x1 x2 y)))))

(define lazy-test
  (mlet ((u coin)
         (x (pure (delay (twiddle u)))))
    (put stdout (pure u))
    (if (tails? u)
        (pure "*")
        (alet ((x1 (force x))
               (x2 (force x))
               (y coin))
          (string-append x1 x2 y)))))

(define shared-test
  (mlet ((u coin)
         (x (share 0 (twiddle u))))
    (put stdout (pure u))
    (if (tails? u)
        (pure "*")
        (alet ((y coin))
          (string-append x x y)))))

(cond-expand
 (chibi)
 (guile (test-begin "hansei")))

(define strict-test-result
  (output-string stdout (collect strict-test)))

(alet ((log strict-test-result))
  (test "strict test (value)"
        ;; duplicated "ax" and "ay"
        '((1/2 . "*")
          (1/8 . "axaxa")
          (1/8 . "axaxb")
          (1/8 . "ayaya")
          (1/8 . "ayayb"))
        (writer-log-value log))
  (test "strict test (output)"
        ;; $ printed twice in heads branch and twice in tails branch
        "$a$a$b$b"
        (writer-log-output log)))

(define thunk-test-result
  (output-string stdout (collect thunk-test)))

(alet ((log thunk-test-result))
  (test "thunk test (value)"
        ;; no more guarantee of duplication
        '((1/2 . "*")
          (1/16 . "axaxa")
          (1/16 . "axaxb")
          (1/16 . "axaya")
          (1/16 . "axayb")
          (1/16 . "ayaxa")
          (1/16 . "ayaxb")
          (1/16 . "ayaya")
          (1/16 . "ayayb"))
        (writer-log-value log))
  (test "thunk test (output)"
        ;; $ printed 12 times, in heads branch only
        "a$$$$$$$$$$$$b"
        (writer-log-output log)))

;; Because of the nature of the embedding here vs. in hansei, there seems to
;; be no way to write a test which makes real use of Scheme's lazy features;
;; the uninterpreted DSL term is memoized rather than its result, and so this
;; test equals the previous one.

 (define lazy-test-result
   (output-string stdout (collect lazy-test)))

(test "lazy test"
      thunk-test-result
      lazy-test-result)

(define shared-test-result
  (output-string stdout (memoize < (collect shared-test))))

(alet ((log shared-test-result))
  (test "shared test (value)"
        ;; same distribution as original strict test
        '((1/2 . "*")
          (1/8 . "axaxa")
          (1/8 . "axaxb")
          (1/8 . "ayaya")
          (1/8 . "ayayb"))
        (writer-log-value log))
  (test "shared test (output)"
        ;; $ printed 2 times, in heads branch only
        "$a$ab"
        (writer-log-output log)))

(cond-expand
 (chibi)
 (guile (test-end "hansei")))
