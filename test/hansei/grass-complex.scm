(import (scheme base)
        (scheme write)
        (effects base)
        (effects functor)
        (effects syntax)
        (effects logic)
        (effects probability)
        (effects io)
        (effects sharing))

;; Distributions

(define (bern weight)
  (flip weight (pure #t) (pure #f)))

;; Execution trace

(define stdout (make-prompt-tag 'stdout))

(define (trace str)
  (put stdout (pure str)))

;; Inference

(define (write-line str)
  (write-string str)
  (newline))

(define (abj p q)
  (and p (not q)))

(define (odds req)
  (output-yield
   stdout
   write-line
   (collect-distribution-sorted
    abj
    req)))

;; Model components

(define obs-cloudy?
  (sequence
    (trace "random cloudy")
    (bern 1/2)))

(odds obs-cloudy?)

(define (obs-rain? obs-cloudy?)
  (mlet ((cloudy? obs-cloudy?))
    (if cloudy?
        (sequence
          (trace "cloudy rain")
          (bern 4/5))
        (sequence
          (trace "clear rain")
          (bern 1/5)))))

(odds (obs-rain? obs-cloudy?))

(define (obs-sprinkler? obs-cloudy?)
  (alet ((weekly?
          (sequence
            (trace "weekly sprinkler")
            (bern 1/7)))
         (random+non-cloudy?
          (sequence
            (trace "random non-cloudy sprinkler")
            (alet ((random? (bern 1/2))
                   (cloudy? obs-cloudy?))
              (and random?
                   (not cloudy?))))))
    (or weekly?
        random+non-cloudy?)))

(odds (obs-sprinkler? obs-cloudy?))

(define (obs-wet-roof? obs-rain?)
  (alet ((rain? (sequence
                  (trace "rain wet roof")
                  obs-rain?))
         (dried? (sequence
                   (trace "random dry roof")
                   (bern 3/10))))
    (and rain?
         (not dried?))))

(odds (obs-wet-roof? (obs-rain? obs-cloudy?)))

(define (obs-wet-grass? obs-rain? obs-sprinkler?)
  (alet ((rain/sprinkler?
          (sequence
            (trace "rain or sprinkler wet grass")
            (alet ((rain? obs-rain?)
                   (sprinkler? obs-sprinkler?))
              (or rain? sprinkler?))))
         (dried?
          (sequence
            (trace "random dry grass")
            (bern 1/10))))
    (and rain/sprinkler?
         (not dried?))))

(odds
 (obs-wet-grass?
  (obs-rain? obs-cloudy?)
  (obs-sprinkler? obs-cloudy?)))

(define grass-model-strict
  (mlet ((cloudy? obs-cloudy?)
         (rain? (obs-rain? (pure cloudy?)))
         (sprinkler? (obs-sprinkler? (pure cloudy?)))
         (wet-roof? (obs-wet-roof? (pure rain?)))
         (wet-grass? (obs-wet-grass?
                      (pure rain?)
                      (pure sprinkler?))))
    (if wet-grass?
        (pure rain?)
        (fail))))

(display (odds grass-model-strict))
(newline)

;; random cloudy
;; cloudy rain
;; random non-cloudy sprinkler
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random non-cloudy sprinkler
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; clear rain
;; random non-cloudy sprinkler
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random non-cloudy sprinkler
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; weekly sprinkler
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; random dry roof
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass
;; rain wet roof
;; random dry grass
;; rain or sprinkler wet grass
;; rain or sprinkler wet grass

(define grass-model-lazy
  (mlet ((pr-cloudy? (later obs-cloudy?))
         (pr-rain? (later (obs-rain? pr-cloudy?)))
         (pr-wet-roof? (later (obs-wet-roof? pr-rain?)))
         (wet-grass? (obs-wet-grass?
                      pr-rain?
                      (obs-sprinkler? pr-cloudy?))))
    (if wet-grass?
        pr-rain?
        (fail))))

(begin
  (display (odds (now grass-model-lazy)))
  (newline))

;; random dry grass
;; rain or sprinkler wet grass
;; random non-cloudy sprinkler
;; random cloudy
;; weekly sprinkler
;; cloudy rain
;; cloudy rain
;; weekly sprinkler
;; cloudy rain
;; cloudy rain
;; weekly sprinkler
;; clear rain
;; clear rain
;; weekly sprinkler
;; clear rain
;; clear rain
;; rain or sprinkler wet grass
;; random non-cloudy sprinkler
;; random cloudy
;; weekly sprinkler
;; cloudy rain
;; cloudy rain
;; weekly sprinkler
;; cloudy rain
;; cloudy rain
;; weekly sprinkler
;; clear rain
;; clear rain
;; weekly sprinkler
;; clear rain
;; clear rain
