;;; This module tests an implementation of dyadic sessions using effects.

;;; Global sessions describe a communication protocol between two parties, in
;;; terms of the primitives `await' and `reply'.  The same session expression
;;; is then evaluated by two separate handlers representing the two parties,
;;; in this case a client and a server, to produce a pair of effects
;;; expressions which are dual with respect to communication flow.  One party
;;; interprets `await' as input and `reply' as output, while the other party
;;; interprets these in the opposite directions.

;;; To run the resulting pair of expressions, we use Guile's Fibers library.
;;; In this interpretation, sending is mapped to `put-message' and receiving
;;; is mapped to `get-message'.  The test at the end of this file is
;;; essentially the same as the one shown in the Fibers README file, except
;;; that it executes the embedded client/server session terms.

(import (scheme base)
        (effects base)
        (effects functor)
        (effects syntax)
        (effects io)
        (effects partial)
        (effects logic)
        (effects session))

;;; Global session terms

(define session1
  (select 'add
          (abegin
            (await 'int)
            (await 'int)
            (reply 'int))))

(define session2
  (abegin
    (select 'mode (await 'cmd))
    (branch
     (select 'add
             (abegin
               (await 'int)
               (await 'int)
               (reply 'int)))
     (select 'reverse
             (abegin
               (await 'str)
               (reply 'str))))))

;;; Formatted I/O primitives

(define i/o-failure (make-prompt-tag 'i/o))

(define wire-port (make-prompt-tag 'wire))  ; network port
(define tty-port (make-prompt-tag 'tty))    ; local port

(define (send-string port str)
  (put port (pure str)))

(define (send-integer port int)
  (put port (pure (number->string int))))

(define (send-command port cmd)
  (put port (pure (symbol->string cmd))))

(define (receive-string port)
  (get port))

(define (receive-integer port)
  (satisfy number? i/o-failure (fmap string->number (get port))))

(define (receive-command port)
  (fmap string->symbol (get port)))

(define (prompt-string port)
  (abegin
    (put port (pure "String: "))
    (receive-string port)))

(define (prompt-integer port)
  (abegin
    (put port (pure "Integer: "))
    (receive-integer port)))

(define (prompt-command port)
  (abegin
    (put port (pure "Command: "))
    (receive-command port)))

;;; Dispatch on data type labels

(define (send-data lbl port x)
  (cond
   ((eq? lbl 'str) (send-string port x))
   ((eq? lbl 'int) (send-integer port x))
   ((eq? lbl 'cmd) (send-command port x))
   (else
    (throw i/o-failure `("Sending unknown type" ,lbl)))))

(define (receive-data lbl port)
  (cond
   ((eq? lbl 'str) (receive-string port))
   ((eq? lbl 'int) (receive-integer port))
   ((eq? lbl 'cmd) (receive-command port))
   (else
    (throw i/o-failure `("Receiving unknown type" ,lbl)))))

(define (prompt-data lbl port)
  (cond
   ((eq? lbl 'str) (prompt-string port))
   ((eq? lbl 'int) (prompt-integer port))
   ((eq? lbl 'cmd) (prompt-command port))
   (else
    (throw i/o-failure `("Prompting unknown type" ,lbl)))))

;;; Stack machine primitives used to implement the server

(define stack-prompt (make-prompt-tag 'stack))

(define (stack-push x)
  (put stack-prompt
    (alet ((lst (get stack-prompt)))
      (cons x lst))))

(define (stack-pop)
  (mlet ((lst (get stack-prompt)))
    (put stack-prompt (pure (cdr lst)))
    (pure (car lst))))

;;; Stack operations

(define (stack-add)
  (alet ((x (satisfy number? stack-prompt (stack-pop)))
         (y (satisfy number? stack-prompt (stack-pop))))
    (+ x y)))

(define (stack-reverse)
  (alet ((str (satisfy string? stack-prompt (stack-pop))))
    (string-reverse str)))

(define (stack-combine op)
  (cond
   ((eq? op 'add) (stack-add))
   ((eq? op 'reverse) (stack-reverse))
   (else
    (throw stack-prompt `("Unknown stack operation" ,op)))))

;;; Client and server handlers

(define (client req)
  (handle-session
   (lambda (op lbl await?)
     (if await?
         (mlet ((x (prompt-data lbl tty-port)))
           (send-data lbl wire-port x)
           (pure
            (make-action-result (if (eq? op 'mode) x op)
                                unit)))
         (alet ((x (receive-data lbl wire-port)))
           (make-action-result op x))))
   req))

(define (server req)
  (handle-session
   (lambda (op lbl await?)
     (if await?
         (mlet ((x (receive-data lbl wire-port)))
           (stack-push x)
           (pure
            (make-action-result (if (eq? op 'mode) x op)
                                unit)))
         (mlet ((x (stack-combine op)))
           (send-data lbl wire-port x)
           (pure (make-action-result op x)))))
   req))

(define (client+server req)
  (values (client req)
          (server req)))

;;; Executing a session and handling I/O errors

(define-record-type <session-exception>
  (make-session-exception)
  session-exception?)

(define-record-type <i/o-exception>
  (make-i/o-exception data)
  i/o-exception?
  (data i/o-exception-irritant))

(define (exec-session req)
  (catch i/o-failure
    (catch logic-failure
      (once req)
      (lambda (_)
        (pure (make-session-exception))))
    (lambda (x)
      (pure (make-i/o-exception x)))))

;;; Updating the stack

(define-record-type <stack-exception>
  (make-stack-exception op)
  stack-exception?
  (op stack-exception-operation))

(define (update-stack req)
  (catch stack-prompt
    (update-state stack-prompt '() req)
    (lambda (op)
      (pure (make-stack-exception op)))))

;;; We use fibers for parallel execution

(import (fibers) (fibers channels))

;;; Connecting local and network ports

(define (connect-tty req)
  (update-exchange tty-port read-line write-string req))

(define (connect-wire in out req)
  (update-exchange wire-port
                   (lambda ()
                     (get-message in))
                   (lambda (x)
                     (put-message out x))
                   req))

;;; Running both parties in parallel

(define (rendezvous client server)
  (run-fibers
   (lambda ()
     (let ((c2s (make-channel))
           (s2c (make-channel)))
       (spawn-fiber (lambda () (connect-wire c2s s2c server)))
       (connect-wire s2c c2s client)))))

(define (run-session session)
  (let-values (((client server) (client+server session)))
    (rendezvous (connect-tty (exec-session client))
                (update-stack (exec-session server)))))
