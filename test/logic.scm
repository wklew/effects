;;; logict.scm --- Logic tests from the LogicT paper

;;; See Kiselyov, Shan & Friedman, 2005

(import (scheme base)
        (scheme case-lambda)
        (effects base)
        (effects syntax)
        (effects partial)
        (effects logic))

(cond-expand
 (chibi (import (chibi test)))
 (guile (import (srfi srfi-64))
        (define (test name x y)
          (test-equal name x y))))

;; Some test expressions

(define t1
  (fail))

(define t2
  (branch 10 (branch 20 30)))

(define t3
  (choice 10 20 30))

;; Running them and observing their results

(cond-expand
 (chibi)
 (guile (test-begin "logic")))

(test "test term 1"
      (choice->list t1)
      '())

(test "test term 2"
      (choice->list t2)
      '(10 20 30))

(test "test term 3"
      (choice->list t3)
      '(10 20 30))

;; An infinite generator of odd numbers

(define odds
  (branch 1
          (mlet ((n odds))
            (+ n 2))))

;; Observing a finite number

(test "infinite stream"
      (choice->list (logic-take 5 odds))
      '(1 3 5 7 9))

(define (even n)
  (satisfy even? logic-failure n))

;; Even though `t3' produces even numbers, the following diverges without
;; finding a single answer

;; (choice->list (logic-take 1 (even (branch odds t3)))

;; This is because `branch' is left-biased; `odds' will produce an infinite
;; stream of observe without ever checking the right-hand side

;; Instead we must use the fair variant, `fair-branch'

(test "fair disjunction 1"
      (choice->list (logic-take 10 (fair-branch odds t3)))
      '(1 10 3 20 5 30 7 9 11 13))

;; Which allows the same program to converge on an answer

(test "fair disjunction 2"
      (choice->list (logic-take 1 (even (fair-branch odds t3))))
      '(10))

;; The same problem arises in the naive use of `bind'; we demonstrate why with
;; the help of the following function

(define (odds+ n)
  (mlet ((m odds))
    (+ n m)))

;; The following diverges, despite `(odds+ 1)` yielding an
;; infinite stream of even numbers

;; (choice->list
;;  (logic-take
;;   1
;;   (even
;;    (bind (branch 0 1)
;;      odds+))))

;; Again, the fair operator `fair-bind' allows the same program to converge

(test "fair conjunction"
      (choice->list
       (logic-take
        1
        (even
         (fair-bind (branch 0 1)
                    odds+))))
      '(2))

;; The next section uses the following helper function to enumerate a range of
;; positive integers

(define (enum-range n m)
  (let loop ((i (- m 1))
             (acc '()))
    (if (< i n)
        acc
        (loop (- i 1)
              (cons i acc)))))

;; And an effectful function which asserts that a number is non-prime

(define (assert-composite n)
  (mlet ((d (list->choice (enum-range 2 n))))
    (assert logic-failure (zero? (modulo n d)))))

;; With which we can enumerate composite numbers

(test "composites"
      (choice->list
       (logic-take
        10
        (mlet ((n odds))
          (assert logic-failure (> n 1))
          (assert-composite n)
          n)))
      '(9 15 15 21 21 25 27 27 33 33))

;; And, inversely, primes

(test "primes"
      (choice->list
       (logic-take
        10
        (mlet ((n odds))
          (assert logic-failure (> n 1))
          (logic-invert (assert-composite n))
          n)))
      '(3 5 7 11 13 17 19 23 29 31))

(cond-expand
 (chibi)
 (guile (test-end "logic")))
