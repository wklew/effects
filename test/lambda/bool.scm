(import (scheme base)
        (effects base)
        (effects io)
        (effects lambda)
        (effects syntax))

(cond-expand
 (chibi (import (chibi test)))
 (guile (import (srfi srfi-64))
        (define (test name x y)
          (test-equal name x y))))

(define id
  (lam (x)
    (var x)))

(define neg
  (lam (p)
    (lift not (var p))))

(define conj
  (lam (p q)
    (mlet ((pv (var p))
           (qv (var q)))
      (and pv qv))))

(define inv
  (lam (f p)
    (app (var f)
      (app neg (var p)))))

(test "identity" #t (val (app id (pure #t))))

(test "negation" #f (val (app neg (pure #t))))

(test "conjunction" #f (val (app conj (pure #t) (pure #f))))

(test "inversion" #t (val (app inv neg (pure #t))))
