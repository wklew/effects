(import (scheme base)
        (effects base)
        (effects lambda))

(cond-expand
 (chibi (import (chibi test)))
 (guile
  (import (srfi srfi-64))
  (begin
    (define (test name x y)
      (test-equal name x y)))))

(test "normalize"
      3
      (val
       (app (norm
             (lam (x y)
               (app (var x) (var y))))
            (lam (x) (var x))
            3)))
