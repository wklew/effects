(import (scheme base)
        (scheme write)
        (effects base)
        (effects logic)
        (effects lambda)
        (effects io)
        (effects syntax))

(cond-expand
 (chibi (import (chibi test)))
 (guile
  (import (srfi srfi-64))
  (begin
    (define (test name x y)
      (test-equal name x y)))))

(test "pure"
      5
      (val
        (def x 5)
        (var x)))

(test "lambda"
      3
      (val
        (def id* (lam (x) (var x)))
        (app (var id*) 3)))

(test "nondet"
      '(#t #f)
      (val
        (def coin #t)
        (def coin #f)
        (choice->list (var coin))))

(test "nondet-lambda"
      '(2 3)
      (val
       (def id*
         (lam (x)
           (var x)))
       (def id*
         (lam (x)
           (mlet ((n (var x)))
             (+ n 1))))
       (choice->list (app (var id*) 2))))

;; => 2 3

(test "recursive"
      '(1 2 3 4)
      (writer-log-output
       (output-list
        'out
        (val
          (def rec
            (lam (x)
              (bind (var x)
                (lambda (n)
                  (if (= n 0)
                      (unit)
                      (mbegin
                       (put 'out n)
                       (app (var rec)
                            (- n 1))))))))
          (app (var rec) 4)))))
