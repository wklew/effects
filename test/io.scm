(import (scheme base)
        (scheme write)
        (effects base)
        (effects io)
        (effects derived-io)
        (effects sharing)
        (effects partial)
        (effects syntax))

;;; Channels over which input/output is communicated

(define in-channel (make-prompt-tag 'in-channel))
(define out-channel (make-prompt-tag 'out-channel))
(define log-channel (make-prompt-tag 'log-channel))

;;; Primitive DSL terms

(define cat
  (put out-channel (get in-channel)))

(define log-begin
  (put log-channel (pure "begin")))

(define log-end
  (put log-channel (pure "end")))

;;; Test term composed of primitives arranged by a combinator

(define t0
  (abegin log-begin cat cat cat log-end))

;;; Resources which actually send and receive data

(define in-string "hello\nworld\n")
(define out-file "out.txt")

;;; Pipes representing ports shared across channels

(define in-pipe (make-prompt-tag 'in-pipe))
(define out-pipe (make-prompt-tag 'out-pipe))

;;; Sequence of terms yielded by partial evaluations of the test term

(define t1
  ;; Read from in-string along in-channel using in-pipe
  (read-input-from-string in-pipe in-channel in-string t0))

(define t2
  ;; Write to out-file along out-channel using out-pipe
  (write-output-to-file out-pipe out-channel out-file t1))

(define t3
  ;; Write to out-file along log-channel using out-pipe
  (write-output-to-file out-pipe log-channel out-file t2))

(define t4
  ;; Handle the shared port for in-pipe
  (handle-sharing in-pipe t3))

(define t5
  ;; Handle the shared port for out-pipe
  (handle-sharing out-pipe t4))

(define t6
  ;; Handle the internal I/O logging
  (output-for-each stdout display t5))

(define t7
  ;; Lift I/O exceptions to Scheme errors
  (catch eof-failure
    t6
    (lambda (_)
      (pure (error "ERROR: end of file")))))
