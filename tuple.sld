;;; (effects tuple) --- Effectful tuples

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library provides a simple interface for constructing effectful pairs
;;; or tuples, such as effect requests labeled with Scheme values.

;;; Code:

(define-library (effects tuple)

  (export label
          annotation-handler
          tuple-handler
          node-label
          node-value
          tuple-fold
          tuple-list
          tuple-string
          tuple-sum
          tuple-product)

  (import (scheme base)
          (scheme case-lambda)
          (effects base))

  (begin

    (define tuple-prompt (make-prompt-tag 'tuple))

    (define-record-type <annotation>
      (make-annotation tag val req)
      annotation?
      (tag annotation-prompt)
      (val annotation-value)
      (req annotation-request))

    (define label
      (case-lambda
        ((tag val req)
         (label tag val req identity))
        ((tag val req cont)
         (request tuple-prompt (make-annotation tag val req)
           cont))))

    (define (annotation-handler proc)
      (make-handler tuple-prompt
        (lambda (cont ann)
          (proc cont
                (annotation-prompt ann)
                (annotation-value ann)
                (annotation-request ann)))))

    (define (tuple-handler tag loop proc)
      (annotation-handler
       (lambda (cont tag* val req)
         (if (eq? tag tag*)
             (proc cont val req)
             (loop (label tag* val req))))))

    (define-record-type <node>
      (make-node lbl val)
      node?
      (lbl node-label)
      (val node-value))

    (define (tuple-fold tag combine base req)
      (let loop ((acc base)
                 (req req))
        (handle req
          `(,(tuple-handler
              tag
              (lambda (req)
                (loop acc req))
              (lambda (cont val req)
                (loop (bind acc
                        (lambda (lbl)
                          (combine val lbl)))
                      (bind req cont)))))
          (lambda (x)
            (bind acc
              (lambda (lbl)
                (make-node lbl x)))))))

    (define (tuple-list tag req)
      (tuple-fold tag cons '() req))

    (define (tuple-string tag req)
      (tuple-fold tag string-append "" req))

    (define (tuple-sum tag req)
      (tuple-fold tag + 0 req))

    (define (tuple-product tag req)
      (tuple-fold tag * 1 req))))
