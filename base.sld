;;; (effects base) --- Scheme effects and handlers core library

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of effects.
;;;
;;; Effects is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Effects is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with effects.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This library defines the core effects implementation, namely:
;;;
;;; - Effect request encapsulation
;;; - Efficient queue representation for composing Kleisli arrows
;;; - Operations for introducing and eliminating (handling) effects
;;; - Means of combining effects

;;; Code:

(define-library (effects base)

  (export identity compose
          call respond unit bind sequence lift)

  (import (scheme base)
          (scheme lazy)
          (containers sequence))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    (define (identity x) x)

    (define (compose f g)
      (lambda (x)
        (f (g x))))

    ;; Effect request encapsulation

    (define-record-type <impure>
      (impure op arr)
      impure?
      (op impure-operation)
      (arr impure-arrow))

    ;; Reified Kleisli arrows

    (define arrow-suspend sequence-single)

    (define arrow-extend sequence-snoc)

    (define (arrow-link req arr)
      (if (impure? req)
          (impure (impure-operation req)
                  (sequence-append (impure-arrow req)
                                   arr))
          (arrow-resume arr req)))

    (define (arrow-resume arr x)
      (let-values (((cont tail) (sequence-uncons arr)))
        (let ((req (cont x)))
          (if (sequence-empty? (force tail))
              req
              (arrow-link req (force tail))))))

    ;; Introducing and eliminating effects

    (define (call op cont)
      (impure op (arrow-suspend cont)))

    (define (respond req pur imp)
      (if (impure? req)
          (imp (impure-operation req)
               (let ((arr (impure-arrow req)))
                 (lambda (x)
                   (arrow-resume arr x))))
          (pur req)))

    ;; Composing effects in series

    (define (unit) unit)

    (define (bind req cont)
      (if (impure? req)
          (impure (impure-operation req)
                  (arrow-extend cont (impure-arrow req)))
          (cont req)))

    (define (sequence . reqs)
      (fold (lambda (req acc)
              (bind acc (lambda (_) req)))
            unit
            reqs))

    ;; Lifted application

    (define (lift proc . reqs)
      (fold (lambda (req acc)
              (bind req
                (lambda (x)
                  (lambda args (apply acc x args))))
              (lambda args (apply proc acc)))
            proc
            reqs))))
