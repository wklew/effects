(define-library (effects logic arithmetic)

  (export zero
          succ
          pred
          genu
          enc
          dec
          add
          mul
          div)

  (import (scheme base)
          (scheme write)
          (effects base)
          (effects functor)
          (effects syntax)
          (effects logic)
          (effects partial)
          (effects lambda)
          (effects io))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    ;; natural number encoding

    (define zero
      (pure '()))

    (define (succ req)
      (lift (lambda (lst)
              (cons 'u lst))
            req))

    (define (pred req)
      (lift cdr req))

    ;; Generating nats

    (define %genu
      (lam* (rec _)
        (choose (list zero (succ (app (var rec) (stop)))))))

    (define genu (app fix %genu (stop)))

    (val (collect-choice (logic-take 3 genu)))

    ;; => (pure (() (u) (u u)))

    ;; convert between encodings

    (define (enc n)
      (once (logic-drop n genu)))

    (val (enc 4))

    ;; => (pure (u u u u))

    (define (dec req)
      (lift length req))

    (dec (pure '(u u)))

    ;; => (pure 2)

    ;; Debug output

    (define (out req)
      (put 'stdout req))

    (define (run-out req)
      (output-yield 'stdout display req))

    ;; Logical summation

    (define %add-base
      (lam* (rec x y z)
        (out (pure "\t"))
        (out (dec (var x)))
        (out (pure " + "))
        (out (dec (var y)))
        (out (pure " = "))
        (out (dec (var z)))
        (out (pure "\n"))
        (observe logic-failure null? (var x))
        (fleft (unify logic-failure (var y) (var z))
               (out (pure "Success\n")))))

    (define %add-ind
      (lam* (rec x y z)
        (observe logic-failure pair? (var x))
        (observe logic-failure pair? (var z))
        (app (var rec)
             (pred (var x))
             (var y)
             (pred (var z)))))

    (define %add
      (lam* (rec)
        (branch (app %add-base (var rec))
                (app %add-ind (var rec)))))

    (define add
      (sequence (out (pure "Adding\n"))
                (app fix %add)))

    (define (runq name count req)
      (run-out
       (sequence
         (out (pure (string-append "----------\nRunning " name "...\n")))
         (collect-choice (logic-take count (val req))))))

    ;; Query 1: running forwards, variable third argument (addition)

    (define q1
      (lam* (x)
        (sequence (app add (enc 2) (enc 3) (var x))
                  (dec (var x)))))

    (runq "q1(5)" 1 (app q1 (enc 5)))   ; => (pure (5))
    (runq "q1(3)" 1 (app q1 (enc 3)))   ; => (pure ())
    (runq "q1(x)" 1 (app q1 genu))      ; => (pure 5)

    ;; Query 2: running backwards, varying first argument (subtraction)

    (define q2
      (lam* (x)
        (sequence (app add (var x) (enc 3) (enc 5))
                  (dec (var x)))))

    (runq "q2(2)" 1 (app q2 (enc 2)))   ; => (pure (2))
    (runq "q2(3)" 1 (app q2 (enc 3)))   ; => (pure ())
    (runq "q2(x)" 1 (app q2 genu))      ; => (pure (2))

    ;; Query 3: nondeterministic: varying first and second arguments

    (define q3
      (lam* (x y)
        (sequence (app add (var x) (enc 3) (var y))
                  (lift list (dec (var x)) (dec (var y))))))

    (runq "q3(2,5)" 1 (app q3 (enc 2) (enc 5))) ; => (pure ((2 5)))
    (runq "q3(2,x)" 1 (app q3 (enc 2) genu))    ; => (pure ((2 5)))
    (runq "q3(x,y)" 1 (app q3 genu genu))       ; => (pure ((0 3)))

    ;; these all diverge

    ;; (runq 2 (app q1 genu))
    ;; (runq 2 (app q2 genu))
    ;; (runq 2 (app q3 (enc 2) genu))
    ;; (runq 1 (app q3 genu (enc 2)))
    ))
